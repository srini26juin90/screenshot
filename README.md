## Prerequisites
1. Node installed on your machine => v10
2. NPM installed on your machine => 5.6.0

## Installing node modules
cd ramesh
npm install

## Run the server
npm start