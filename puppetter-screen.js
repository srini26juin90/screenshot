'use strict';

const puppeteer = require('puppeteer');

const puppetterScreen = async function(req, res) {
     try {
          console.log("req........",req);
          const browser = await puppeteer.launch();
          const page = await browser.newPage();
          await page.goto('https://google.com');
          // await page.screenshot({path: 'example.png'});
          const base64 = await page.screenshot({ encoding: "base64" })
          console.log(base64)
          await browser.close();
     } catch (error) {
          console.log("Error :::", error);
     }
};

module.exports = puppetterScreen;