'use strict';

const express = require("express");
const puppeteer = require('puppeteer');
const puppetterScreen = require("./puppetter-screen");

const app = express();

app.use('/', puppetterScreen);

app.listen(3128);